#include<iostream>
using namespace std;
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};
class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        ListNode *cur;
        ListNode *pre;
        cur = head->next;
        pre = head;
        while(cur!=nullptr){
            pre->next = cur->next;
            cur->next = pre;
            if(cur->next->next!=nullptr){
                cur = cur->next->next->next;
            }
            if(pre!=nullptr){
                pre = pre->next;
            }   
        }
    }
};
int main()
{
    ListNode *node1;
    ListNode *node2;
    ListNode *node3;
    ListNode *node4;
    node1 = new ListNode(1, node2);
    node2 = new ListNode(2, node3);
    node3 = new ListNode(3, node4);
    node4 = new ListNode(4);
}