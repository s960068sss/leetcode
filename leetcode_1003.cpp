#include<iostream>
#include<stack>
using namespace std;
class Solution {
public:
    // bool isValid(string s) {
    //     string s_left, s_right;
    //     for(int i=0;i<s.length();){
    //         // cout<<"i : "<<i<<'\n';
    //         if(s[i]=='a' && s[i+1]=='b' && s[i+2]=='c'){
    //             if(i==0){
    //                 s_left = "";
    //             }
    //             else{
    //                 s_left = s.substr(0, i);
    //             }
    //             if(i+3>=s.length()){
    //                 s_right="";
    //             }
    //             else{
    //                 s_right = s.substr(i+3, s.length());
    //             }
    //             // cout<<"left : "<<s_left<<'\n';
    //             // cout<<"right : "<<s_right<<'\n';
    //             s = s_left+s_right;
    //             // cout<<s<<'\n';
    //         }
    //         else{
    //             i++;
    //         }

    //         if(s.length()==0){
    //             return true;
    //         }
    //     }
    //     return false;
    // }
    bool isValid(string s) {
        stack<char> my_stack;
        char a, b, c;
        for(int i=0;i<s.length();i++){
            my_stack.push(s[i]);
            if(my_stack.size()<3){
                continue;
            }
            else{
                c = my_stack.top();
                my_stack.pop();
                b = my_stack.top();
                my_stack.pop();
                a = my_stack.top();
                my_stack.pop();
                // cout<<a<<b<<c<<'\n';
                if(a=='a' && b=='b' && c=='c'){
                    continue;
                }
                else{
                    my_stack.push(a);
                    my_stack.push(b);
                    my_stack.push(c);
                }
            }
            
        }
        if(my_stack.size()==0){
            return true;
        }
        else{
            return false;
        }
    }
};
int main()
{
    Solution a;
    bool ans;
    string s;
    cin>>s;
    ans = a.isValid(s);
    cout<<ans<<'\n';
}