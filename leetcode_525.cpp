#include<iostream>
#include<vector>
using namespace std;


class Solution {
public:
    int findMaxLength(vector<int>& nums) {
        int n = nums.size();
        int *arr;
        arr = new int[2*n+5];
        for(int i=0;i<2*n+5;i++){
            arr[i] = -10;
        }
        int max = -10;
        int count = 0;
        int tmp = 0;
        arr[n] = -1;
        for(int i=0;i<n;i++){
            if(nums[i]==0){
                tmp-=1;
            }
            else{
                tmp+=1;
            }
            if(arr[tmp+n]==-10){
                arr[tmp+n] = i;
            }
            else{
                count = i - arr[tmp+n];
                if(max<count){
                    max = count;
                }
            }
        }
        if(max==-10){
            return 0;
        }
        return max;
    }
};
int main()
{
    int n;
    int tmp;
    cin>>n;
    vector<int> nums;
    for(int i=0;i<n;i++){
        cin>>tmp;
        nums.push_back(tmp);
    }
    Solution a;
    int ans;
    ans = a.findMaxLength(nums);
    cout<<ans<<'\n';
}
