#include<iostream>
#include<vector>
using namespace std;
class Solution {
public:
    vector<int> countBits(int n) {
        vector<int> vec;
        vector<int> ans;
        vec.push_back(0);
        int pos = 0;
        int count = 0;
        ans.push_back(0);
        for(int i=1;i<=n;i++){
            pos = 0;
            while(1){
                if(vec[pos]==1){
                    count--;
                    vec[pos]=0;
                    pos++;
                    if(pos==vec.size()){
                        vec.push_back(0);
                    }
                    
                }
                else{
                    count++;
                    vec[pos] = 1;
                    ans.push_back(count);
                    break;
                }
                
            }
        }
        return ans;
    }
};
int main()
{
    int n;
    vector<int>ans;
    cin>>n;
    Solution sol;
    ans = sol.countBits(n);
    for(int i=0;i<ans.size();i++){
        cout<<ans[i]<<' ';
    }
    cout<<'\n';
}