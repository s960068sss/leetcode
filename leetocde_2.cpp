#include<iostream>
#include<string>

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};
 
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        int add = 0;
        ListNode* ans, *cur;
        ans = nullptr;
        while(l1 || l2){
            ListNode* node = new ListNode();
            if(ans==nullptr){
                ans = node;
                cur = node;
            }
            else{
                cur->next = node;
                cur = cur->next;
            }
            if(l1==nullptr){
                node->val = l2->val+add;
                add=0;
                l2 = l2->next;
            }
            else if(l2==nullptr){
                node->val = l1->val+add;
                add=0;
                l1 = l1->next;
            }
            else{
                node->val = l1->val+l2->val+add;
                add=0;
                l1 = l1->next;
                l2 = l2->next;
            }
            if(node->val>=10){
                add=1;
                node->val = (node->val)%10;
            }
        }
        if(add==1){
            ListNode* node = new ListNode(1);
            cur->next = node;
        }
        return ans;
    }
};