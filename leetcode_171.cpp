#include<iostream>
#include<string>
#include<math.h>
using namespace std;
class Solution {
public:
    int titleToNumber(string columnTitle) {
        int ans=0;
        for(int i=0;i<columnTitle.length();i++){
            ans+=(columnTitle[i]-'A'+1)*pow(26, columnTitle.length()-i-1);
        }
        return ans;
    }
};
int main()
{
    string a;
    Solution sol;
    int ans;
    cin>>a;
    ans = sol.titleToNumber(a);
    // cout<<('A'-'A'+1)*pow(26,1)<<'\n';
    cout<<ans<<'\n';
}