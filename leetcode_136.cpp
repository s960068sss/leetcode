#include<iostream>
#include<vector>
using namespace std;
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        // int add[70000];
        // for(int i=0;i<70000;i++){
        //     add[i] = 0;
        // }
        // for(int i=0;i<nums.size();i++){
        //     add[nums[i]+30005]++;
        // }
        // for(int i=0;i<70000;i++){
        //     if(add[i]==1){
        //         return i-30005;
        //     }
        // }
        int ans = 0;
        for(int i=0;i<nums.size();i++){
            ans = ans^nums[i];
        }
        return ans;
    }
};
int main()
{
    Solution sol;
    int ans;
    int n;
    int a;
    vector<int> nums;
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>a;
        nums.push_back(a);
    }
    ans = sol.singleNumber(nums);
    cout<<ans<<'\n';

}