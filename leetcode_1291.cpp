#include<iostream>
#include<vector>
#include<string.h>
#include<math.h>
using namespace std;
class Solution {
public:
    vector<int> sequentialDigits(int low, int high) {
        string low_str = to_string(low);
        string high_str = to_string(high);
        int low_length = low_str.length();
        int high_length = high_str.length();
        int digit = low_length;
        // vector<char> tmp;
        vector<int> ans;
        int num = 0;
        int flag = 0;
        while(digit < high_length+1){
            // tmp.clear();
            if(flag==1){
                break;
            }
            for(int i=1;i<=10-digit;i++){
                num = 0;
                for(int j=0;j<digit;j++){
                    // tmp.push_back(('0'+i+j));
                    num+=(i+j)*pow(10, (digit-j-1));
                }
                // cout<<num<<'\n';
                if(num>=low && num<=high){
                    ans.push_back(num);
                }
                if(num>high){
                    flag=1;
                    break;
                }
            }
            digit++;
        }
        return ans;

    }
};
int main()
{
    Solution a;
    vector<int> ans;
    int low, high;
    cin>>low>>high;
    ans = a.sequentialDigits(low, high); 
    for(int i=0;i<ans.size();i++){
        cout<<ans[i]<<' ';
    }
    cout<<'\n';
    return 0;
}