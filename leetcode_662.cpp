#include<iostream>
#include<vector>
#include<math.h>
#include<queue>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    queue<pair<TreeNode*, long long int> > q;
    int widthOfBinaryTree(TreeNode* root) {
        q.push({root, 0});
        long long int start=0, end=0;
        long long int max_len = -100;
        while(!q.empty()){
            int n = q.size();
            for(int i=0;i<n;i++){
                TreeNode *node = q.front().first;
                long long int val = q.front().second;
                if(i==0){
                    start = val;
                }
                val-=start;
                if(i==n-1){
                    end = val;
                }
                cout<<val<<' ';
                q.pop();
                if(node->left!=nullptr){
                    q.push({node->left, val*2+1});
                }
                if(node->right){
                    q.push({node->right, val*2+2});
                }
                max_len = (max_len<end)?end:max_len;
            }
            cout<<'\n';
        }
        return max_len+1;
    }
};