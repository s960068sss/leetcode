#include <iostream>
#include <vector>
using namespace std;
class Solution
{
public:
    vector<bool> visit;
    bool validateBinaryTreeNodes(int n, vector<int> &leftChild, vector<int> &rightChild)
    {
        int root = find_root(n, leftChild, rightChild);
        cout<<"root found : "<<root<<'\n';
        if(root==-1){
            return false;
        }
        for(int i=0;i<n;i++){
            visit.push_back(0);
        }
        bfs(root, n, leftChild, rightChild);
        for(int i=0;i<n;i++){
            if(visit[i]==0){
                return false;
            }
        }
        return true;

    }
    int find_root(int n, vector<int> &leftChild, vector<int> &rightChild){
        vector<int> num_parent;
        for(int i=0;i<n;i++){
            num_parent.push_back(0);
        }
        for(int i=0;i<n;i++){
            if(leftChild[i]==-1){
                continue;
            }
            if(leftChild[leftChild[i]]==i){
                return -1;
            }
            num_parent[leftChild[i]]++;
        }
        for(int i=0;i<n;i++){
            if(rightChild[i]==-1){
                continue;
            }
            if(rightChild[rightChild[i]]==i){
                return -1;
            }
            num_parent[rightChild[i]]++;
        }
        int num_zero = 0;
        int root = -1;
        for(int i=0;i<n;i++){
            if(num_parent[i]>1){
                return -1;
            }
            if(num_parent[i]==0){
                num_zero++;
                root = i;
            }
            if(num_zero>1){
                return -1;
            }
        }
        if(num_zero==0){
            return -1;
        }
        else{
            return root;
        }
    }
    void bfs(int node, int n, vector<int> &leftChild, vector<int> &rightChild){
        if(node==-1){
            return;
        }
        visit[node]=1;
        bfs(leftChild[node], n, leftChild, rightChild);
        bfs(rightChild[node], n, leftChild, rightChild);
    }
};
int main()
{
    Solution a;
    int n;
    int tmp;
    vector<int> leftchild, rightchild;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        cin >> tmp;
        leftchild.push_back(tmp);
    }
    for (int i = 0; i < n; i++)
    {
        cin >> tmp;
        rightchild.push_back(tmp);
    }
    bool ans;
    ans = a.validateBinaryTreeNodes(n, leftchild, rightchild);
    cout<<ans<<'\n';
}