#include<iostream>
#include<vector>
using namespace std;
class Solution {
public:
    vector<vector<int> >my_vec;
    vector<vector<int> > combinationSum(vector<int>& candidates, int target) {
        vector<int> temp_vec;
        sum(candidates, target, temp_vec, 0);
        return my_vec;
    }
    void sum(vector<int>& candidates, int target, vector<int> temp_vec, int k){
        if(target==0){
            my_vec.push_back(temp_vec);
        }
        if(target<0){
            return ;
        }
        for(int i=k;i<candidates.size();i++){
            temp_vec.push_back(candidates[i]);
            sum(candidates, target-candidates[i], temp_vec, i);
            temp_vec.pop_back();
        }
    }
};
int main()
{
    vector<int> candidate;
    int target;
    int n;
    int a;
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>a;
        candidate.push_back(a);
    }
    cin>>target;
    Solution sol;
    vector<vector<int> > ans;
    ans = sol.combinationSum(candidate, target);
    for(int i=0;i<ans.size();i++){
        for(int j=0;j<ans[i].size();j++){
            cout<<ans[i][j]<<" ";
        }
        cout<<'\n';
    }
}