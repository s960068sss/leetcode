#include<iostream>
#include<vector>
#include<string>
using namespace std;
class Solution {
public:
    int compareVersion(string version1, string version2) {
        vector<string> s1, s2;
        string a="";
        for(int i=0;i<version1.length();i++){
            if(version1[i]!='.'){
                if(!(a.length()==0 && version1[i]=='0')){
                    a+=version1[i];
                }
            }
            if(version1[i]=='.' || i==version1.length()-1){
                if(a.length()==0){
                    a += '0';
                }
                s1.push_back(a);
                a = "";
            }
        }
        a="";
        for(int i=0;i<version2.length();i++){
            if(version2[i]!='.'){
                if(!(a.length()==0 && version2[i]=='0')){
                    a+=version2[i];
                }
            }
            if(version2[i]=='.' || i==version2.length()-1){
                if(a.length()==0){
                    a += '0';
                }
                s2.push_back(a);
                a = "";
            }
        }
        int i=0;
        int one, two;
        while(i<s1.size() && i<s2.size()){
            one = stoi(s1[i]);
            two = stoi(s2[i]);
            if(one<two){
                return -1;
            }
            if(one>two){
                return 1;
            }
            i++;
        }
        if(i==s1.size()){
            for(int j=i;j<s2.size();j++){
                two = stoi(s2[j]);
                if(two!=0){
                    return -1;
                }
            }
        }
        else{
            for(int j=i;j<s1.size();j++){
                one = stoi(s1[j]);
                if(one!=0){
                    return 1;
                }
            }
        }
        return 0;
    }
};
int main()
{
    string a, b;
    cin>>a>>b;
    Solution sol;
    int ans = sol.compareVersion(a,b);
    cout<<ans<<'\n';
}
