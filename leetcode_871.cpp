#include<iostream>
#include<vector>
using namespace std;
class Solution {
public:
    int minRefuelStops(int target, int startFuel, vector<vector<int> >& stations) {
        int N = stations.size();
        // cout<<"shit"<<'\n';
        long long int dp[505];
        for(int i=0;i<=N;i++){
            dp[i] = 0;
        }
        dp[0] = startFuel;
        for(int i=0;i<N;i++){
            for(int j=i;j>=0;j--){
                if(dp[j]>=stations[i][0]){
                    dp[j+1] = max(dp[j+1], dp[j]+stations[i][1]); 
                }
            }
        }
        
        for(int i=0;i<=N;i++){
            if(dp[i]>=target){
                return i;
            }
        }
        return -1;
    }
};
int main(){
    Solution a;
    int ans;
    int n;
    int target, startFuel;
    cin>>n>>target>>startFuel;
    vector<vector<int> >stations;
    vector<int>temp;
    int c, d;
    for(int i=0;i<n;i++){
        cin>>c>>d;
        temp.clear();
        temp.push_back(c);
        temp.push_back(d);
        stations.push_back(temp);
    }
    ans = a.minRefuelStops(target, startFuel, stations);
    cout<<ans<<'\n';
}
