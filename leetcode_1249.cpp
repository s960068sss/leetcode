#include<iostream>
#include<string>
using namespace std;


class Solution {
public:
    string minRemoveToMakeValid(string s) {
        int left;
        int right;
        left = 0;
        right = 0;
        int i=0;
        while(i<s.length()){
            if(s[i]=='('){
                left++;
                i++;
            }
            else if(s[i]==')'){
                if(left!=0){
                    left--;
                    i++;
                }
                else{
                    s.erase(i,1);
                }
            }
            else{
                i++;
            }
        }
        i = s.length()-1;
        while(i>=0){
            if(s[i]=='(' && left!=0){
                s.erase(i,1);
                left--;
            }
            i--;
            
        }
        return s;
    }
};
int main()
{
    string s, ans;
    cin>>s;
    Solution sol;

    ans = sol.minRemoveToMakeValid(s);
    cout<<ans<<'\n';
}
