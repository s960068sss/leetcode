#include<iostream>
#include<vector>
#include<stack>
using namespace std;
class Solution {
public:
    bool validateStackSequences(vector<int>& pushed, vector<int>& popped) {
        stack<int> my_stack;
        int pos = 0;
        for(int i=0;i<pushed.size();i++){
            my_stack.push(pushed[i]);
            while(!my_stack.empty()   && popped[pos]==my_stack.top()){
                pos++;
                cout<<pos<<'\n';
                if(pos==popped.size()){
                    return true;
                }
                my_stack.pop();
            }
        }
        return false;
        
    }
};