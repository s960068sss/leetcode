#include<iostream>
#include<vector>
using namespace std;
class Solution {
public:
    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
        vector<int> arr;
        vector<int>max_arr;
        // cout<<"in arr : "<<'\n';
        int check = 0;
        for(int i=0;i<gas.size();i++){
            arr.push_back(gas[i]-cost[i]);
            // cout<<arr[i]<<" ";
            check+=arr[i];

            max_arr.push_back(-1);
        }
        
        // cout<<'\n';
        if(check<0){
            return -1;
        }
        int max = 0;
        int max_index = 0;
        int all = 0;
        int n = 0;
        int j;
        n = arr.size();
        if(n==0){
            return 0;
        }
        int flag = 0;
        for(int i=0;i<arr.size();i++){
            if(flag==1){
                break;
            }
            max = -100;
            max_index = i;
            all = 0;
            n = arr.size();
            j = i;
            while(n--){
                all+=arr[j];
                if(all>=max){
                    max = all;
                    max_index = j;
                    max_arr[i] = max;
                }
                if(all<0){
                    max_arr[i] = all;
                    break;
                }
                j++;
                if(j>=arr.size()){
                    flag=1;
                }
                j = j % arr.size();
            }
            i = max_index;
        }
        max = -10;
        max_index = 0;
        for(int i=0;i<max_arr.size();i++){
            if(max_arr[i]>max){
                max = max_arr[i];
                max_index = i;
            }
        }
        if(max<0){
            return -1;
        }
        else{
            return max_index;
        }
    }
};
int main(){
    Solution a;
    int sol;
    vector<int> gas, cost;
    int n;
    int tmp;
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>tmp;
        gas.push_back(tmp);
    }
    for(int i=0;i<n;i++){
        cin>>tmp;
        cost.push_back(tmp);
    }
    sol = a.canCompleteCircuit(gas, cost);
    cout<<sol<<'\n';
}
