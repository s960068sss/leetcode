#include<iostream>
#include<vector>
using namespace std;
class Solution {
public:
    string smallestStringWithSwaps(string s, vector<vector<int>>& pairs) {
        vector<vector<int> >vec;
        int flag=0;
        for(int i=0;i<pairs.size();i++){
            flag=0;
            for(int j=0;j<vec.size();j++){
                for(int k=0;k<vec[j].size();k++){
                    if(vec[j][k]==pairs[i][0] || vec[j][k]==pairs[i][1]){
                        flag=1;
                        if(vec[j][k]==pairs[i][0]){
                            vec[j].push_back(pairs[i][1]);
                        }
                        else{
                            vec[j].push_back(pairs[i][0]);
                        }
                    }
                }
            }
            if(flag==0){
                vector<int> tmp;
                tmp.push_back(pairs[i][0]);
                tmp.push_back(pairs[i][1]);
                vec.push_back(tmp);
            }
            
        }
    }
};
int main(){
    int n;
    cin>>n;
    vector<int> tmp;
    vector<vector<int> >vec;
    string s;
    int a, b;
    cin>>s;
    for(int i=0;i<n;i++){
        cin>>a>>b;
        tmp.push_back(a);
        tmp.push_back(b);
        vec.push_back(tmp);
    }
    Solution sol;
    sol.smallestStringWithSwaps(s, vec);
    
}