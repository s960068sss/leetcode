#include<iostream>
using namespace std;
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};
class Solution {
public:
    bool hasCycle(ListNode *head) {

        
        //method 1
        // int count = 0;
        // while(head!=nullptr){
        //     head = head->next;
        //     count++;
        //     if(count>100005){
        //         return true;
        //     }
        // }
        // return false;


        //method 2
        // ListNode *slow = head;
        // ListNode *fast = head;
        // while(fast!=nullptr){
        //     if(fast->next){
        //         fast = fast->next->next;
        //     }
        //     else{
        //         break;
        //     }
        //     slow = slow->next;
        //     if(fast==slow){
        //         return true;
        //     }
        // }
        // return false;
    }
};
int main()
{

}