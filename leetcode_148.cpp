#include<iostream>
#include<vector>

using namespace std;
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};
 
class Solution {
public:
    ListNode* sortList(ListNode* head) {
        // method1

        // ListNode *cur = head;
        // vector<int> vec;
        // while(cur!=nullptr){
        //     vec.push_back(cur->val);
        //     cur = cur->next;
        // }
        // cur = head;
        // sort(vec.begin(), vec.end());
        // int i = 0;
        // while(cur!=nullptr){
        //     cur->val = vec[i];
        //     i++;
        //     cur = cur->next;
        // }
        // return head;

        //method2 merge srot
        if(head==nullptr || head->next==nullptr){
            return head;
        }
        ListNode *mid = getmid(head);
        ListNode *left = sortList(head);
        ListNode *right = sortList(mid);
        return merge(left, right);
    }
    ListNode* getmid(ListNode* node){
        ListNode *midpre = nullptr;
        while(node && node->next){
            if(midpre==nullptr){
                midpre=node;
            }
            else{
                midpre = midpre->next;
            }
            node = node->next->next;
        }
        ListNode* mid = midpre->next;
        midpre->next = nullptr;
        return mid;
    }
    ListNode* merge(ListNode* left, ListNode* right){
        ListNode tmp(0);
        ListNode *ptr;
        ptr = &tmp;
        while(left && right){
            if(left->val>right->val){
                ptr->next = right;
                right = right->next;
            }
            else{
                ptr->next = left;
                left = left->next;
            }
            ptr = ptr->next;

        }
        if(left){
            ptr->next = left;
        }
        if(right){
            ptr->next = right;
        }
        return tmp.next;
    }
};

