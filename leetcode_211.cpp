#include<iostream>
#include<vector>
#include <unordered_map>
#include<map>
#include<iterator>
using namespace std;

class WordDictionary {
public:
    // vector<string> my_vec;
    // unordered_map <int, vector<string> > mp;
    map<int, vector<string> > mp;
    WordDictionary() {
        
    }
    
    void addWord(string word) {
        mp[word.length()].push_back(word);
    }
    
    bool search(string word) {
        int length = word.length();
        auto iter = mp.find(length);
        bool flag;
        if(iter==mp.end()){
            return false;
        }
        for(int i=0;i<iter->second.size();i++){
            flag=1;
            for(int j=0;j<length;j++){
                if(word[j]=='.' || (iter->second)[i][j]=='.'){
                    continue;
                }
                if(word[j]!=(iter->second)[i][j]){
                    flag=0;
                }
            }
            if(flag==1){
                cout<<"true"<<' ';
                return true;
            }
        }
//        cout<<"test"<<'\n';
        cout<<"false"<<' ';
        return false;
    }
};



int main(){
    WordDictionary *wordDictionary = new WordDictionary();
    wordDictionary->addWord("a");
    wordDictionary->addWord("b");
    wordDictionary->addWord("c");
//    wordDictionary->addWord("dad");
//    wordDictionary->addWord("mad");
//    wordDictionary->search("pad"); // return False
//    wordDictionary->search("bad"); // return True
//    wordDictionary->search(".ad"); // return True
    wordDictionary->search("a"); // return True
    wordDictionary->search("."); // return True
    wordDictionary->search("d"); // return True
    wordDictionary->search("b"); // return True
    cout<<'\n';
}
