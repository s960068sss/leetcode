#include<iostream>
#include<vector>
using namespace std;
class Node
{
public:
    int val;
    Node *next;
    Node *random;

    Node(int _val)
    {
        val = _val;
        next = NULL;
        random = NULL;
    }
};

class Solution
{
public:
    Node *copyRandomList(Node *head)
    {
        Node* new_head=nullptr;
        int count = 0;
        Node* cur = head;
        while(cur){
            count++;
            cur = cur->next;
        }
        cur = head;
        Node *new_cur = new_head;
        Node *pre = nullptr;
        vector<Node*> vec;
        for(int i=0;i<count;i++){
            Node* node = new Node(cur->val);
            pre = cur;
            cur = cur->next;
            pre->next = node;
            vec.push_back(pre);
            if(new_head==nullptr){
                new_head = node;
                new_cur = new_head;
            }
            else{
                new_cur->next = node;
                new_cur = new_cur->next;
            }
        }
        new_cur = new_head;
        cur = head;
        int i=0;
        while(i<count){
            cur = vec[i];
            if(cur->random){
                new_cur->random = cur->random->next;
            }
            else{
                new_cur->random = nullptr;
            }
            i++;
            new_cur = new_cur->next;
        }
        i = 0;
        while(i<count-1){
            vec[i]->next = vec[i+1];
            i++;
        }
        vec[count-1]->next = nullptr;
        return new_head;
    }
};
int main()
{
    Node * node0 = new Node(7);
    Node * node1 = new Node(13);
    Node * node2 = new Node(11);
    Node * node3 = new Node(10);
    Node * node4 = new Node(1);
    node0->next = node1;
    node1->next = node2;
    node2->next = node3;
    node3->next = node4;
    node0->random = nullptr;
    node1->random = node0;
    node2->random = node4;
    node3->random = node2;
    node4->random = node0;
    Solution sol;
    sol.copyRandomList(node0);
}
