#include<iostream>

using namespace std;
struct TreeNode {
     int val;
     TreeNode *left;
     TreeNode *right;
     TreeNode() : val(0), left(nullptr), right(nullptr) {}
     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 };

class Solution {
public:
    int max_depth = 0;
    int maxDepth(TreeNode* root) {
        int depth;
        bfs(root, 1);
        return max_depth;
    }
    void bfs(TreeNode* node, int depth){
        if(node==nullptr){
            return ;
        }
        if(depth>=max_depth){
            max_depth = depth;
        }
        bfs(node->right, depth+1);
        bfs(node->left, depth+1);
    }
};
int main()
{
    // Solution sol;
    // TreeNode* root;
    // TreeNode* node1;
    // TreeNode* node2;
    // TreeNode* node3;
    // TreeNode* node4;
    // TreeNode* node5;
    // TreeNode* node6;
    // TreeNode* node7;
    // root = new TreeNode(3, node1, node2);
    // node1 = new TreeNode(9);
    // node2 = new TreeNode(20, node3, node3);
    // node3 = new TreeNode();
    // node4 = new TreeNode();
    // int ans = sol.maxDepth(root);
    // cout<<ans<<'\n';
}