
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
    ListNode* rotateRight(ListNode* head, int k) {
        int count = 0;
        
        ListNode *cur = head;
        ListNode *last = nullptr;
        while(cur){
            count++;
            last = cur;
            cur = cur->next;
            
        }
        if(count<=1){
            return head;
        }
        k = k%count;
        if(k==0){
            return head;
        }
        k = count-k;
        
        ListNode *pre = nullptr;
        cur = head;
        while(k--){
            pre = cur;
            cur = cur->next;
        }
        last->next = head;
        pre->next = nullptr;
        head = cur;
        return head;
    }
};