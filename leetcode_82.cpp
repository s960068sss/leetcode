#include<iostream>
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        //method 1
        // ListNode *cur, *pre, *pos;
        // if(!head || !head->next){
        //     return head;
        // }
        // cur = head->next;
        // pre = head;
        // pos = head;
        // while(cur){
        //     if(cur->val==pre->val){
        //         while(cur && cur->val==pre->val ){
        //             cur = cur->next;
        //         }
        //         if(pre==head){
        //             head = cur;
        //         }
        //         else{
        //             pos->next = cur;
        //         }
        //         pre = cur;
        //         if(cur){
        //             cur = cur->next;
        //         }
        //     }
        //     else{
        //         pos = pre;
        //         pre = cur;
        //         cur = cur->next;
        //     }
            
        // }
        // return head;
        //method 2(faster)
        ListNode *cur, *pre;
        cur = pre = head;
        if(!head || !head->next){
            return head;
        }
        while(cur->next){
            if(cur->val==cur->next->val){
                while(cur->next && cur->val==cur->next->val){
                    cur = cur->next;
                }
                if(pre==head && cur->val==pre->val){
                    head = cur->next;
                    pre = head;
                }
                else{
                    pre->next = cur->next;
                }
                if(cur->next){
                    cur = cur->next;
                }
            }
            else{
                pre = cur;
                cur = cur->next;
            }
            
        }
        return head;
    }
};
int main()
{
    ListNode *node1 = new ListNode(1);
    ListNode *node2 = new ListNode(2);
    ListNode *node3 = new ListNode(3);
    ListNode *node4 = new ListNode(3);
    ListNode *node5 = new ListNode(4);
    ListNode *node6 = new ListNode(5);
    ListNode *node7 = new ListNode(5);
    ListNode *node8 = new ListNode(6);
    node1->next = node2;
    node2->next = node3;
    node3->next = node4;
    node4->next = node5;
    node5->next = node6;
    node6->next = node7;
    node7->next = node8;
    Solution sol;
    sol.deleteDuplicates(node1);
}
