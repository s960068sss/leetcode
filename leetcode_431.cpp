#include<iostream>
#include<vector>
using namespace std;
class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& nums) {
        int arr[5005];
        for(int i=0;i<5005;i++){
            if(i<3){
                arr[i] = 0;
            }
            else{
                arr[i] = arr[i-1]+i-2;
            }
        }
        int ans = 0;
        int count = 2;
        for(int i=2;i<nums.size();i++){
            if(nums[i]-nums[i-1]==nums[i-1]-nums[i-2]){
                count++;
            }
            else{
                ans+=arr[count];
                count = 2;
            }
        }
        ans+=arr[count];
        return ans;
    }
};
int main()
{
    int n;
    int a;
    cin>>n;
    vector<int>nums;
    for(int i=0;i<n;i++){
        cin>>a;
        nums.push_back(a);
    }
    Solution sol;
    int ans = sol.numberOfArithmeticSlices(nums);
    cout<<ans<<'\n';
}