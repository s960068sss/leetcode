#include<iostream>
#include<vector>
using namespace std;
//  Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    vector<int> ans;
    vector<int> getAllElements(TreeNode* root1, TreeNode* root2) {
        traverse(root1);
        traverse(root2);
        // int arr[200005];
        // for(int i=0;i<200005;i++){
        //     arr[i] = 0;
        // }
        // for(int i=0;i<ans.size();i++){
        //     arr[ans[i]+100000]++;
        // }
        // ans.clear();
        // for(int i=0;i<200005;i++){
        //     for(int j=0;j<arr[i];j++){
        //         ans.push_back(i-100000);
        //     }
        // }
        sort(ans.begin(), ans.end());
        return ans;
    }
    void traverse(TreeNode* node){
        if(node==NULL){
            return ;
        }
        ans.push_back(node->val);
        traverse(node->left);
        traverse(node->right);
    }
};
int main(){
    Solution a;
    vector<int> ans;
    TreeNode *root1, *root2, *node1,*node2,*node3,*node4;
    root1 = new TreeNode(2);
    root2 = new TreeNode(1);
    node1 = new TreeNode(1);
    node2 = new TreeNode(4);
    node3 = new TreeNode(0);
    node4 = new TreeNode(3);
    root1->left = node1;
    root1->right = node2;
    root2->left = node3;
    root2->right = node4;
    ans = a.getAllElements(root1, root2);
    for(int i=0;i<ans.size();i++){
        cout<<ans[i]<<' ';
    }
    cout<<'\n';
}
