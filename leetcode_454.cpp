#include<iostream>
#include<vector>
#include<map>
using namespace std;

class Solution {
public:
    int fourSumCount(vector<int>& nums1, vector<int>& nums2, vector<int>& nums3, vector<int>& nums4) {
        int n = nums1.size();
        int count = 0;
        map<int, int> mp;
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                mp[nums1[i]+nums2[j]]++;
            }
        }
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                count += mp[-(nums3[i]+nums4[j])];
            }
        }
        
        return count;
    }
};
int main(){
    vector<int> nums1, nums2, nums3, nums4;
    Solution a;
    int ans;
    int n;
    int tmp;
    cin>>n;
    
    for(int j=0;j<n;j++){
        cin>>tmp;
        nums1.push_back(tmp);
    }
    for(int j=0;j<n;j++){
        cin>>tmp;
        nums2.push_back(tmp);
    }
    for(int j=0;j<n;j++){
        cin>>tmp;
        nums3.push_back(tmp);
    }
    for(int j=0;j<n;j++){
        cin>>tmp;
        nums4.push_back(tmp);
    }
    
    ans = a.fourSumCount(nums1, nums2, nums3, nums4);
    cout<<ans<<'\n';
}