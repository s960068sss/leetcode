#include<iostream>
#include<map>
#include<vector>
using namespace std;
class Solution {
public:
    int majorityElement(vector<int>& nums) {
        //method 1

        // map<int, int> mp;
        // // mp[nums[1]]++;
        // for(int i=0;i<nums.size();i++){
        //     // mp.insert(pair<int, int>(nums[i], mp[nums[i]]+1));
        //     mp[nums[i]]++;
        //     if(mp[nums[i]]>nums.size()/2){
        //         return nums[i];
        //     }
        // }
        // return 0;

        //method 2
        int count = 0;
        int candidate = 0;
        for(int i=0;i<nums.size();i++){
            if(count == 0){
                candidate = nums[i];
            }
            if(nums[i]==candidate){
                count--;
            }
            else{
                count++;
            }
        }
        return candidate;
    }
};
int main(){
    int n;
    cin>>n;
    vector<int> nums;
    int a;
    for(int i=0;i<n;i++){
        cin>>a;
        nums.push_back(a);
    }
    Solution sol;
    int ans = sol.majorityElement(nums);
    cout<<ans<<'\n';
}