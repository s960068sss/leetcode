#include<iostream>
#include<vector>
#include<math.h>
using namespace std;
class Solution {
public:
    double champagneTower(int poured, int query_row, int query_glass) {
        double arr[105][105];
        for(int i=0;i<105;i++){
            for(int j=0;j<105;j++){
                arr[i][j] = 0;
            }
        }
        double q;
        arr[0][0] = poured;
        for(int i=0;i<=query_row;i++){
            for(int j=0;j<=i;j++){
                q = (arr[i][j]-1)/2;
                if(q>0){
                    arr[i+1][j]+=q;
                    arr[i+1][j+1]+=q;
                }
            }
        }
        if(arr[query_row][query_glass]>=1){
            return 1;
        }
        else{
            return arr[query_row][query_glass];
        }
    }
};
int main(){
    int a, b, c;
    cin>>a>>b>>c;
    double ans;
    Solution sol;
    ans = sol.champagneTower(a, b, c);
    cout<<ans<<'\n';
}
