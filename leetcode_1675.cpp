#include<iostream>
#include<vector>
#include<queue>

using namespace std;
#define INT_MAX 1000000005
class Solution {
public:
    int minimumDeviation(vector<int>& nums) {
        priority_queue<int> pq;
        int min = INT_MAX;
        for(int i=0;i<nums.size();i++){
            if(nums[i]%2){
                nums[i]*=2;
            }
            pq.push(nums[i]);
            if(min>nums[i]){
                min = nums[i];
            }
        }
        vector<int> ans;
        int ans_min = INT_MAX;
        ans_min = (pq.top()-min);
        while(pq.top()%2==0){
            int max = pq.top();
            pq.pop();
            max/=2;
            if(min>max){
                min = max;
            }
            pq.push(max);
            if(ans_min>pq.top()-min){
                ans_min = pq.top()-min;
            }
        }
        return ans_min;
    }
};
int main(){
    int n;
    vector<int> nums;
    cin>>n;
    int a;
    for(int i=0;i<n;i++){
        cin>>a;
        nums.push_back(a);
    }
    Solution sol;
    int ans;
    ans = sol.minimumDeviation(nums);
    cout<<ans<<'\n';
}