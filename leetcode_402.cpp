#include<iostream>
#include<string>
using namespace std;
class Solution {
public:
    string removeKdigits(string num, int k) {
        int len = num.length();
        string s = "";
        int pos = 0;
        char min = '9';
        int flag=0;
        for(int i=0;i<len-k;i++){
            min = '9';
            for(int j=pos;j<=k+i;j++){
                if(min>num[j]){
                    min = num[j];
                    pos = j;
                }
            }
            pos++;
            if(min!='0'){
                flag=1;
            }
            if(flag==1){
                s+=min;
            }
        }
        if(s.length()==0){
            s="0";
        }
        return s;
    }
};
int main()
{
    Solution sol;
    string ans, num;
    int k;
    cin>>num>>k;
    ans = sol.removeKdigits(num, k);
    cout<<ans<<'\n';
}