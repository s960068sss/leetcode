#include<iostream>
#include<vector>
using namespace std;
class Solution {
public:
    
    vector<vector<int> > subsets(vector<int>& nums) {
        vector<int>tmp;
        vector<vector<int> >ans;
        ans.push_back(tmp);
        int sz;
        for(int i=0;i<nums.size();i++){
            sz = ans.size();
            for(int j=0;j<sz;j++){
                tmp = ans[j];
                tmp.push_back(nums[i]);
                ans.push_back(tmp);
            }
        }
        return ans;
    }

};
int main(){
    Solution sol;
    vector<int> num;
    int n;
    int a;
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>a;
        num.push_back(a);
    }
    vector<vector<int> >ans;
    ans = sol.subsets(num);
    for(int i=0;i<ans.size();i++){
        for(int j=0;j<ans[i].size();j++){
            cout<<ans[i][j]<<' ';
        }
        cout<<'\n';
    }

}