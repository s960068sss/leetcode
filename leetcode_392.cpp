#include<iostream>
#include<string>
using namespace std;
class Solution {
public:
    bool isSubsequence(string s, string t) {
        int s_pos, t_pos;
        s_pos = 0;
        t_pos = 0;
        while(s_pos!=s.length() && t_pos!=t.length()){
            if(s[s_pos]==t[t_pos]){
                s_pos++;
            }
            t_pos++;
        }
        if(s_pos==s.length()){
            return true;
        }
        else{
            return false;
        }
    }
};
int main()
{
    string s, t;
    Solution sol;
    cin>>s>>t;
    bool ans = sol.isSubsequence(s,t);
    cout<<ans<<'\n';
}