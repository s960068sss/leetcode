#include<iostream>
#include<vector>
#include<string>
using namespace std;
class Solution {
public:
    string simplifyPath(string path) {
        vector<string> vec;
        string name;
        for(int i=0;i<path.length()-1;){
            if(path[i]=='/'){
                i++;
                name="";
                while(i<path.length() && path[i]!='/'){
                    name+=path[i];
                    i++;
                }
                if(name==".." && vec.size()>0){
                    vec.pop_back();
                }
                else if(name.length()!=0 && name!="." & name!=".."){
                    vec.push_back(name);
                }   
            }
            else{
                i++;
            }
        }
        string ans="";
        if(vec.size()==0){
            ans = "/";
        }
        else{
            for(int i=0;i<vec.size();i++){
                ans+='/'+vec[i];
            }
        }
        return ans;
        

    }
};


