#include<iostream>
#include<vector>
using namespace std;

// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> neighbors;
    Node() {
        val = 0;
        neighbors = vector<Node*>();
    }
    Node(int _val) {
        val = _val;
        neighbors = vector<Node*>();
    }
    Node(int _val, vector<Node*> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};


class Solution {
public:
    int visit[105];
    map<int, Node*> mp;
    Node* cloneGraph(Node* node) {
        if(node==nullptr){
            return node;
        }
        for(int i=0;i<105;i++){
            visit[i]=0;
        }
        build_pool(node);
        for(int i=0;i<105;i++){
            visit[i]=0;
        }
        insert(node);
        return mp[1];
    }
    void build_pool(Node *node){
        if(node==nullptr){
            return;
        }
        if(visit[node->val]==0){
            visit[node->val] = 1;
            Node* tmp = new Node(node->val);
            mp[node->val] = tmp;
            for(int i=0;i<node->neighbors.size();i++){
                build_pool(node->neighbors[i]);
            }
        }
    }
    void insert(Node* node){
        if(node==nullptr){
            return;
        }
        if(visit[node->val]==1){
            return;
        }
        visit[node->val] = 1;
        Node* tmp = mp[node->val];
        for(int i=0;i<node->neighbors.size();i++){
            tmp->neighbors.push_back(mp[node->neighbors[i]->val]);
            insert(node->neighbors[i]);
        }
    }
};
int main()
{
    Node* node1 = new Node(1);
    Node* node2 = new Node(2);
    Node* node3 = new Node(3);
    Node* node4 = new Node(4);
    node1->neighbors.push_back(node2);
    node1->neighbors.push_back(node4);
    node2->neighbors.push_back(node1);
    node2->neighbors.push_back(node3);
    node3->neighbors.push_back(node2);
    node3->neighbors.push_back(node4);
    node4->neighbors.push_back(node1);
    node4->neighbors.push_back(node3);
    Solution sol;
    Node *temp = sol.cloneGraph(node1);
}
